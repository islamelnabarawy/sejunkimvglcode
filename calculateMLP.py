# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 13:17:52 2014

@author: sejunkim
"""

import numpy as np

def feedforward(vInput, dInput, nNodes, mWeight, dOutput):
    """
     A function to calculate a generic MLP neural network output through feedforward pass

     The input parameters are:
      - vInput: input vector
      - dInput: dimension of the input
      - nNodes: # of nodes in the neural network (excludes bias)
      - mWeight: the weight matrix of the neural network (includes bias weight)
      - dOutput: dimension of the output
      
     The return values are:
      - output: output vector
      - a: output of each node (used for backpropagation) 
      - s: output of each node pre activation function (used for backpropagation)
    """

    a = np.append([0.],vInput)
    s = np.array([0.]*(nNodes+1))


    for j in xrange(dInput+1, nNodes+1):

        for m in range(0, j):

            s[j] = s[j]+mWeight[m][j] * a[m]
        a=np.append(a,activationFunction(s[j],0))

    
    y = a[nNodes-dOutput+1:nNodes+1]
    
    return y, a, s

def backpropagation(vInput, dInput, nNodes, mWeight, dOutput):
    """
     A function to calculate a generic MLP neural network output through feedforward pass

     The input parameters are:
      - vInput: input vector
      - dInput: dimension of the input
      - nNodes: # of nodes in the neural network (excludes bias)
      - mWeight: the weight matrix of the neural network (includes bias weight)
      - dOutput: dimension of the output
      
     The return values are:
      - dydx: derivative of neural network in terms of input
      - dydw: derivative of neural network in terms of weights
    """

    _,a,s = feedforward(vInput, dInput, nNodes, mWeight, dOutput)
    dydw = [[0. for _ in xrange(dOutput)] for _ in xrange((nNodes+1)*(nNodes+1))]
    dydx = [[0. for _ in xrange(dOutput)] for _ in xrange(dInput)]
    
    for i in xrange(dOutput):
        lastNodeForThisOutput = nNodes+1-dOutput+i
        
        da = [0.]*(lastNodeForThisOutput+1)
        ds = [0.]*(lastNodeForThisOutput+1)
        for j in reversed(xrange(1, lastNodeForThisOutput + 1)):
            if j==lastNodeForThisOutput:
                da[j] = 1
            else:
                for m in xrange(j+1, lastNodeForThisOutput+1):
                    da[j] = da[j] + mWeight[j][m] * ds[m]
            ds[j]=da[j] * activationFunction(s[j], 1)
            
            for m in xrange(j):
                dydw[j*(nNodes+1)+m][i] = ds[j] * a[m]
        
        for j in xrange(dInput):
            dydx[j][i] = da[j]
    
    
    return dydx, dydw
    

    


def activationFunction(x, mode):
    """
     Returns output of activation function (or derivative)
    
     Input parameters are:
      - x: input scalar
      - mode: 0 when normal, 1 when derivative
     
     Output is:
      - y: output scalar
    """
    
    if mode==0:
        y = np.tanh(x)
    else:
        y= 1 - np.power(np.tanh(x), 2)

    return y
    
    