# -*- coding: utf-8 -*-
"""
Created on Tue Sep 30 10:32:01 2014

@author: sejunkim
"""
import numpy as np

def costFunction(vState, dState, vAction, dAction, time, isTerminal, mode):
    """
     Define cost function (U) here. Output varies by mode(0,1,2).
      - Mode 0 : Returns cost function output
      - Mode 1 : Returns dU/dx (derivative of state) output
      - Mode 2 : Returns dU/du (derivative of action) output

     NOTE: NO MATTER WHAT THE OUTPUT IS (SCALAR or VECTOR), KEEP THE OUTPUTS IN BRACKETS


     The input parameters are:
      - vState: state vector
      - dState: dimension of the state
      - vAction: action vector
      - dAction: dimension of the action
      - time: if time happens to be a feature
      - isTerminal: If termination, returns final instantaneous cost (=1) else (=0) returns regular cost
      - mode: decides which output to be returned (default = 0)
      
     The return value is:
      - output: scalar value (mode=0) or vector (mode=1,2)
    """
    
    if mode>2 or mode<0:
        mode = 0
    if isTerminal>1 or isTerminal<0:
        isTerminal = 0
    
    x = np.array(vState)
    u = np.array(vAction)
    t = time
    
    
    if isTerminal == 0:
        # U(x,u,t) = u0^2 + u1^2
        if mode==0:
            cost = np.square(u[0]) + np.square(u[1])
        elif mode==1: #dU/dx - in a dim(x)*1 matrix
            cost = [[0],\
                    [0]]
        else: #mode==2 #dU/du - in a dim(u)*1 matrix
            cost = [[2 * u[0]],\
                    [2 *u[1]]]
    else: #isTerminal=0
        #U(x,u,t) = x0^2 + x1^2
        if mode==0:
            cost = [np.square(x[0])+ np.square(x[1])]
        elif mode==1: #dU/dx (t=terminal) - in a dim(x)*1 matrix
            cost = [[2*x[0]],\
                    [2*x[1]]]
        else: #mode==2 #dU/du (t=terminal) - in a dim(u)*1 matrix
            cost = [[0],\
                    [0]]

    
        
    
    return cost
    


def modelFunction(vState, dState, vAction, dAction, time, mode):
    """
     Define model function (f) here. Output varies by mode(0,1,2).
      - Mode 0 : Returns cost function output
      - Mode 1 : Returns df/dx (derivative of state) output
      - Mode 2 : Returns df/du (derivative of action) output

     NOTE: NO MATTER WHAT THE OUTPUT IS (SCALAR or VECTOR), KEEP THE OUTPUTS IN BRACKETS

     The input parameters are:
      - vState: state vector
      - dState: dimension of the state
      - vAction: action vector
      - dAction: dimension of the action
      - time: if time happens to be a feature
      - mode: decides which output to be returned (default = 0)
      
     The return value is:
      - output: next state (mode=0) or derivative (mode=1,2)
    """
    
    if mode >2 or mode<0:
        mode= 0
    
    x = np.array(vState)
    u = np.array(vAction)
    t = time
    
    # f(x,u,t) = (x0+u0, x1+u1)
    if mode==0: # in a standard vector [x(0), x(1), ... x(dim(x)-1)]
        output = [x[0] + u[0], x[1]+u[1]]
    elif mode==1: # df/dx - in a dim(x)*dim(x) matrix
        output = [[1,0], \
                  [0,1]]
    else: #mode==2 # df/du - in a dim(u)*dim(x) matrix
        output = [[1,0],\
                  [0,1]]
        
    return output
    


    
    
    