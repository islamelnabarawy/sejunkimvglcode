# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 12:19:50 2014

@author: sejunkim
"""

import setupACD
import calculateMLP as cMLP
import fndef as fd
import numpy as np

input = 0;
if input == 0:
    print('No input - only state transition\n')

n1 = 15
n2 = 15
dState = 2
dAction = 2
w1 = np.array([[0.1 for _ in xrange(n1+1)] for _ in xrange(n1+1)])
w2 = np.array([[-0.1 for _ in xrange(n2+1)] for _ in xrange(n2+1)])

mInitialState = [0.5, -0.5]

fCriticLearningRate = 0.1
fActorLearningRate = 0.1
fDiscountFactor = 1
fLambda = 0.9
numIteration = 500

acd = setupACD.acd_network(dState, dAction, n1, n2, mActorWeight=w1, mCriticWeight=w2)

terminal_t = 2

for iterCount in xrange(numIteration):
    t = 0
    x=np.array([mInitialState])
    u=np.array([]).reshape(0,dAction)
    # {Unroll Trajectory...}
    while t<terminal_t:
        u=np.append(u, [cMLP.feedforward(x[t], acd.dimState, acd.numActorNodes, acd.matrixActorWeight, acd.dimAction)[0]], axis=0)
        nextState = np.array(fd.modelFunction(x[t], dState, u[t], dAction, t, 0));
        x=np.append(x, [[0.8, -0.8]], axis=0)
        
        t = t + 1;
    
    final = t
    vp = np.matrix(fd.costFunction(x[t], dState, _, _, t, 1, 1))#.transpose() # Termination cost
    vDeltaW = np.matrix([0.]*((acd.numCriticNodes+1)*(acd.numCriticNodes+1))).transpose()
    vDeltaZ = np.matrix([0.]*((acd.numActorNodes+1)*(acd.numActorNodes+1))).transpose()
    # {Backward Pass...}
    for t in reversed(xrange(final)): # F-1 --> 0 (step -1)
        # Things used
        mdUdx = np.matrix(fd.costFunction(x[t], dState, u[t], dAction, t, 0, 1))#.transpose()
        #print "mdUdx shape = ", mdUdx.shape
        
        mdUdu = np.matrix(fd.costFunction(x[t], dState, u[t], dAction, t, 0, 2))#.transpose()
        #print "mdUdu shape = ", mdUdu.shape
    
        mdfdx = np.matrix(fd.modelFunction(x[t], dState, u[t], dAction, t, 1))#.transpose()
        #print "mdfdx shape = ", mdfdx.shape
        
        mdfdu = np.matrix(fd.modelFunction(x[t], dState, u[t], dAction, t, 2))#.transpose()
        #print "mdfdu shape = ", mdfdu.shape
        
        mdAdx, mdAdz = cMLP.backpropagation(x[t], acd.dimState, acd.numActorNodes, acd.matrixActorWeight, acd.dimAction)
        mdAdx = np.matrix(mdAdx)
        #print "mdAdx shape = ", mdAdx.shape
        
        mdAdz = np.matrix(mdAdz)
        #print "mdAdz shape = ", mdAdz.shape
        
        mdGdw = np.matrix(cMLP.backpropagation(x[t], acd.dimState, acd.numCriticNodes, acd.matrixCriticWeight, acd.dimState)[1])
        #print "mdGdw shape = ", mdGdw.shape
    
        #~G(t) = vector critic at time=t
        vCriticGt = np.matrix(cMLP.feedforward(x[t], acd.dimState, acd.numCriticNodes, acd.matrixCriticWeight, acd.dimState)[0]).transpose()
        #print "vCriticGt shape = ", vCriticGt.shape
        
        #~G(t+1) = vector critic at time=t+1
        vCriticGt1 = np.matrix(cMLP.feedforward(x[t+1], acd.dimState, acd.numCriticNodes, acd.matrixCriticWeight, acd.dimState)[0]).transpose()
        #print "vCriticGt1 shape = ", vCriticGt1.shape
        
        # Get G' = target value gradient
        vTargetG = mdUdx + fDiscountFactor * mdfdx * vp + mdAdx * (mdUdu + fDiscountFactor * mdfdu * vp)
        
        # Get delta w (for critic weight)
        vDeltaW = vDeltaW + mdGdw * (vTargetG - vCriticGt)
        
        # Get delta z (for actor weight)
        vDeltaZ = vDeltaZ - mdAdz * (mdUdu + fDiscountFactor * mdfdu * vCriticGt1)
    
        # Get new p
        vp = fLambda * vTargetG + (1-fLambda) * vCriticGt
        
    #end for
        
        
    # need to change vDelta into mDelta form
    mDeltaZ = vDeltaZ.reshape(n1+1,n1+1).transpose()
    #mDeltaZ = np.zeros((acd.numActorNodes+1, acd.numActorNodes+1), dtype=float)
    #for i in xrange(acd.numActorNodes+1):
    #    for j in xrange(acd.numActorNodes+1):
    #        mDeltaZ[i][j] = vDeltaZ.item(j * (acd.numActorNodes+1)+i)
    
    # need to change vDelta into mDelta form
    mDeltaW = vDeltaW.reshape(n2+1,n2+1).transpose()
    #mDeltaW = np.zeros((acd.numCriticNodes+1, acd.numCriticNodes+1), dtype=float)
    #for i in xrange(acd.numCriticNodes+1):
    #    for j in xrange(acd.numCriticNodes+1):
    #        mDeltaW[i][j] = vDeltaW.item(j * (acd.numCriticNodes+1)+i)
    
    acd.matrixActorWeight = acd.matrixActorWeight + fActorLearningRate * np.array(mDeltaZ)
    acd.matrixCriticWeight = acd.matrixCriticWeight + fCriticLearningRate * np.array(mDeltaW)

    if np.mod(iterCount, 10)==0 or iterCount==(numIteration-1):
        print "iteration ", iterCount
    if iterCount == (numIteration-1):
        print "x=",x
        print "u=",u
    
    totCost = 0
    for j in xrange(final):
        totCost = totCost + fd.costFunction(x[j], dState, u[j], dAction, j, 0, 0)
    totCost = totCost + fd.costFunction(x[final], dState, _, _, final, 1, 0)
    if np.mod(iterCount, 10)==0 or iterCount==(numIteration-1):
        print "total cost =", totCost
    
    
    
    
    
    




