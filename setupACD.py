# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 10:52:10 2014

@author: sejunkim
"""
import numpy as np




class acd_network:
    """
     Setup the ACD network into a struct including dimensions of state and action and actor / critic characteristics (weights, sizes).
     
     The input parameters are as follows:
      - dState: dimension of the state vector
      - dAction: dimension of the action vector
      - nActorNodes: # of nodes in the actor NN, excluding the bias
      - nCriticNodes: # of nodes in the critic NN, excluding the bias
      - (optional) mActorWeight = weight:
               Weight matrix of the actor. Takes preset weights. 
               If empty, random weights in [-1, 1] range is generated
      - (optional) mCriticWeight = weight
               Weight matrix of the critic. Takes preset weights.
               If empty, random weights in [-1, 1] range is generated
    """

    def __init__(self, dState, dAction, nActorNodes, nCriticNodes, *args, **kwargs):
        self.dimState = dState
        self.dimAction = dAction
        self.numActorNodes = nActorNodes
        self.numCriticNodes = nCriticNodes
                
        initActorWeight = True
        initCriticWeight = True
        
        mActorWeight = kwargs.pop('mActorWeight', None)
        if mActorWeight is not None:
            self.matrixActorWeight = mActorWeight
            initActorWeight = False
        
        mCriticWeight = kwargs.pop('mCriticWeight', None)
        if mCriticWeight is not None:
            self.matrixCriticWeight = mCriticWeight
            initCriticWeight = False
        
        if initActorWeight == True:
            self.matrixActorWeight = np.random.uniform(-1, 1, [nActorNodes+1, nActorNodes+1])
        
        if initCriticWeight == True:
            self.matrixCriticWeight = np.random.uniform(-1, 1, [nCriticNodes+1, nCriticNodes+1])
    
    
    


        
            
        
        
        
        